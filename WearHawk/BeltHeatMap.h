#pragma once

#include <QWidget>
#include <QVBoxLayout>

#include "CVImageWidget.h"


class BeltHeatMap : public QWidget
{
	Q_OBJECT

public:
	BeltHeatMap(QWidget *parent);
	BeltHeatMap(QWidget *parent, int rowWidth, int rowCount);

	~BeltHeatMap();

	void AddRow(std::vector<float> values);
	void SetRowWidth(int rowWidth);
	void SetRowCount(int rowCount);
	void SetMaxVal(float maxVal);
	void SetMinVal(float minVal);

private:

	void UpdateSize();

	int rowWidth = -1;
	int rowCount = -1;
	float maxVal = 20.0;
	float minVal = 10.0;

	cv::Mat im;
	QImage qim;

	int updateRow = 0;
	cv::Mat _tmp;

protected:
	void paintEvent(QPaintEvent* /*event*/);
	
};
