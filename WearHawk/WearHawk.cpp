#include "WearHawk.h"

#include <QFileInfo.h>
#include <opencv2/opencv.hpp>
#include <QTimer.h>
#include <QMetatype.h>
#include <QImage>
#include <QFrame>

WearHawk::WearHawk(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	ui.beltHeatMapWidget->SetMinVal(13.0);
	ui.beltHeatMapWidget->SetMaxVal(35.0);
	ui.beltHeatMapWidget->SetRowWidth(980);
	ui.beltHeatMapWidget->SetRowCount(400);

	liveFeedItem = new CVImageGraphicsItem(QSize(1920, 1080));
	profileFeedItem = new CVImageGraphicsItem(QSize(980, 100));

	scene.addItem(liveFeedItem);
	scene2.addItem(profileFeedItem);
	beltFeedRect = new QGraphicsRectItem(QRectF(0.0, 0.0, ui.graphicsView_liveFeed->size().width(), 30.0));
	beltFeedRect->setBrush(QColor(0x30, 0x30, 0x30, 0xEE));

	beltFeedText = new QGraphicsSimpleTextItem("Belt Feed");
	beltFeedText->setBrush(QColor(0xFF, 0xFF, 0xFF, 0xFF));
	beltFeedText->setScale(1.5);
	beltFeedText->setPos(10, 7);

	QPen ROIPen = QPen(QColor("red"));
	ROIPen.setWidthF(2.5);
	beltROIRect = new QGraphicsRectItem(QRectF(0.0, 0.0, 0.0, 0.0));
	beltROIRect->setPen(ROIPen);
	beltROIRect->setBrush(Qt::NoBrush);

	scene.addItem(beltFeedRect);
	scene.addItem(beltFeedText);
	scene.addItem(beltROIRect);

	profileFeedLabelRect = new QGraphicsRectItem(QRectF(0.0, 0.0, ui.graphicsView_profileFeed->size().width(), 30.0));
	profileFeedLabelRect->setBrush(QColor(0x30, 0x30, 0x30, 0xEE));

	profileFeedLabelText = new QGraphicsSimpleTextItem("Zoomed Profile ROI");
	profileFeedLabelText->setBrush(QColor(0xFF, 0xFF, 0xFF, 0xFF));
	profileFeedLabelText->setScale(1.5);
	profileFeedLabelText->setPos(10, 7);

	profilePathItem = new QGraphicsPathItem();
	QPen profilePathPen = QPen(QColor("yellow"));
	profilePathItem->setBrush(Qt::NoBrush);
	profilePathPen.setWidthF(2.5);
	profilePathItem->setPen(profilePathPen);

	// Try anti-aliasing.
	ui.graphicsView_profileFeed->setRenderHint(QPainter::Antialiasing);

	profileROIRect = new QGraphicsRectItem(QRectF(0.0, 0.0, 0.0, 0.0));
	profileROIRect->setPen(ROIPen);
	profileROIRect->setBrush(Qt::NoBrush);


	scene2.addItem(profilePathItem);
	scene2.addItem(profileFeedLabelRect);
	scene2.addItem(profileFeedLabelText);
	scene2.addItem(profileROIRect);

	ui.graphicsView_liveFeed->setScene(&scene);
	ui.graphicsView_profileFeed->setScene(&scene2);



	// This bit is here so that the frame can be passed between threads in Qt automatically. 
	// Otherwise there's a queueing issue...
	qRegisterMetaType<cv::Mat>("cv::Mat");

	CaptureWorker* captureWorker;
	bool useCamera = false;

	if (useCamera) {
		captureWorker = new CaptureWorker(0);
	}
	else {
		QString applicationPath = QCoreApplication::applicationDirPath();
		captureWorker = new CaptureWorker(QString(applicationPath + "/../../Videos/00003.MTS").toStdString());
	}
	
	captureWorker->moveToThread(&captureThread);
	connect(&captureThread, &QThread::finished, captureWorker, &QObject::deleteLater);
	connect(captureWorker, SIGNAL(newFrame(const cv::Mat &)), this, SLOT(ProcessNewFrame(const cv::Mat &)));
	connect(this, &WearHawk::operate, captureWorker, &CaptureWorker::Start);

	captureThread.start();
	
	emit operate();

}

void WearHawk::keyPressEvent(QKeyEvent *e) {
	this->close();
}

void WearHawk::ProcessNewFrame(const cv::Mat &frame) {

	qDebug() << "WearHawk: Processing frame...";
	myFrame = frame.clone();

	// Sort out the belt feed view.
	ui.graphicsView_liveFeed->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	ui.graphicsView_liveFeed->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	beltFeedRect->setRect(QRectF(0.0, 0.0, ui.graphicsView_liveFeed->viewport()->size().width() - 2, 30.0));

	ui.graphicsView_liveFeed->fitInView(QRectF(0, 0, 1920, 1080));

	beltFeedRect->resetTransform();
	beltFeedText->resetTransform();
	beltFeedRect->setTransform(ui.graphicsView_liveFeed->transform().inverted());
	beltFeedText->setTransform(ui.graphicsView_liveFeed->transform().inverted());

	beltFeedText->setPos(ui.graphicsView_liveFeed->mapToScene(QPoint(10, 5)));

	liveFeedItem->showFrame(frame);


	// Sort out the ROI
	cv::Size s = myFrame.size();
	
	//cv::Rect smallROI(0, 0, 600, 100);
	//cv::Mat image_roi = myFrame(smallROI);

	const Rect BELT_ROI(460, 520, 980, 100);
	cv::Mat image_roi = myFrame(BELT_ROI);

	// Implement the profile feed view.
	ui.graphicsView_profileFeed->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	ui.graphicsView_profileFeed->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	profileFeedLabelRect->setRect(QRectF(0.0, 0.0, ui.graphicsView_profileFeed->viewport()->size().width() - 2, 30.0));

	ui.graphicsView_profileFeed->fitInView(QRectF(0, 0, 980, 100));

	profileFeedLabelText->resetTransform();
	profileFeedLabelRect->resetTransform();

	profileFeedLabelText->setTransform(ui.graphicsView_profileFeed->transform().inverted());
	profileFeedLabelRect->setTransform(ui.graphicsView_profileFeed->transform().inverted());

	profileFeedLabelText->setPos(ui.graphicsView_profileFeed->mapToScene(QPoint(10, 5)));

	beltROIRect->setRect(QRectF(460, 520, 980, 100));

//	beltROIRect->pen().setWidthF(1.5 * 1080.0 / ui.graphicsView_profileFeed->height());


	profileFeedItem->showFrame(image_roi);

	// Sort out the heatmap
	BeltProfileOutput out = myProfiler.processFrame(myFrame);

	ui.beltHeatMapWidget->AddRow(out.thickness);

//	ui.profileFeedWidget->showImage(image_roi);

	// Add the profile 
	QPainterPath path;
	path.moveTo(0, out.thickness[0] + out.pulleyVector[0] - 520);
	
	for (qreal i = 1; i < out.thickness.size(); i++) {
		path.lineTo(i, out.thickness[i] + out.pulleyVector[i] - 520.0 + 8.0);
//		path.lineTo(i, 50);
	}

	profilePathItem->setPath(path);
	profilePathItem->update();

	profileROIRect->setRect(QRectF(0,0, 980+2, 100+2));

}

