#pragma once
#include <QGraphicsItem>
#include <QImage>
#include <QPainter>
#include <opencv2/opencv.hpp>
#include <QWidget>

class CVImageGraphicsItem : public QGraphicsItem
{
public:
	CVImageGraphicsItem(QSize size, QGraphicsItem *parent = 0) : QGraphicsItem(parent) {
		this->size = size;
	}

	QRectF boundingRect() const Q_DECL_OVERRIDE;
//	QPainterPath shape() const Q_DECL_OVERRIDE;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget) Q_DECL_OVERRIDE; 
	QWidget *w;

private:
	QSize size;
	QImage _qimage;
	cv::Mat _tmp;

protected:
//	void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
//	void mouseMoveEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
//	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;

public slots :

	void showFrame(const cv::Mat& image);

};