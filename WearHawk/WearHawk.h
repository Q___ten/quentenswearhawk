#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_WearHawk.h"

#include <QMediaPlayer.h>
#include <QMediaPlaylist.h>
#include <QFileInfo.h>
#include <QThread.h>

//#include <QVideoWidget.h>

#include "CaptureWorker.h"
#include "BeltProfiler.h"
#include "CVImageGraphicsItem.h"
#include <QGraphicsScene>

//#include "CVGLCanvas.h"

class WearHawk : public QMainWindow
{
    Q_OBJECT

public:
    WearHawk(QWidget *parent = Q_NULLPTR);

private:
    Ui::WearHawkClass ui;

	QMediaPlayer* player;
	QMediaPlaylist* playlist;

	CaptureWorker* captureWorker;
	QThread captureThread;

	cv::Mat myFrame;

	BeltProfiler myProfiler;

	CVImageGraphicsItem *liveFeedItem;
	CVImageGraphicsItem *profileFeedItem;
	QGraphicsScene scene;
	QGraphicsScene scene2;
	//	QGraphicsPixmapItem pix;
	QGraphicsRectItem *beltFeedRect;
	QGraphicsSimpleTextItem *beltFeedText;

	QGraphicsRectItem *profileFeedLabelRect;
	QGraphicsSimpleTextItem *profileFeedLabelText;

	QGraphicsPathItem *profilePathItem;

	QGraphicsRectItem *beltROIRect;
	QGraphicsRectItem *profileROIRect;

protected slots:

void keyPressEvent(QKeyEvent *event);

private slots:

	void ProcessNewFrame(const cv::Mat &frame);

signals:

	void operate();

};
