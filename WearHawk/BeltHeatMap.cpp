#include "BeltHeatMap.h"


BeltHeatMap::BeltHeatMap(QWidget *parent = 0)
	: QWidget(parent)
{
}

BeltHeatMap::~BeltHeatMap()
{

}


void BeltHeatMap::AddRow(std::vector<float> values) {
	
	int i = 0;
	for each (float val in values)
	{
		uchar uval = 0;
		if (val > maxVal) {
			uval = 255;
		}else if (val > 0) {
			uval = (uchar)((val - minVal) / (maxVal - minVal) * 255);
		}

		im.at<uchar>(updateRow, i) = uval;
		i++;
	}

	repaint();

	updateRow++;
	updateRow = updateRow % rowCount;

}

void BeltHeatMap::SetRowWidth(int rowWidth) {
	this->rowWidth = rowWidth;
	UpdateSize();
}

void BeltHeatMap::SetRowCount(int rowCount) {
	this->rowCount = rowCount;
	UpdateSize();
}

void BeltHeatMap::SetMaxVal(float maxVal) {
	this->maxVal = maxVal;
}

void BeltHeatMap::SetMinVal(float minVal) {
	this->minVal = minVal;
}

void BeltHeatMap::UpdateSize() {
	if (rowCount > 0 && rowWidth > 0){
		im.create(rowCount, rowWidth, CV_8UC1);
		// Do we need to zero it?
		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < rowWidth; j++) {
				im.at<uchar>(i, j) = 0;
			}
		}
	}
}

void BeltHeatMap::paintEvent(QPaintEvent* /*event*/){
	// Display the image
	QPainter painter(this);

	QSize widgetSize = this->size();

	QRectF rectangle(0.0, 0.0, widgetSize.width() - 1, widgetSize.height() - 1);

	QPen pen;
	pen.setColor("orange");
	painter.setPen(pen);
	painter.drawRect(rectangle);

	cv::Mat colorIm;
	applyColorMap(im, colorIm, cv::COLORMAP_JET);

	switch (colorIm.type()) {
	case CV_8UC1:
		cv::cvtColor(colorIm, _tmp, CV_GRAY2RGB);
		break;
	case CV_8UC3:
		cv::cvtColor(colorIm, _tmp, CV_BGR2RGB);
		break;
	}

	// QImage needs the data to be stored continuously in memory
	assert(_tmp.isContinuous());
	// Assign OpenCV's image buffer to the QImage. Note that the bytesPerLine parameter
	// (http://qt-project.org/doc/qt-4.8/qimage.html#QImage-6) is 3*width because each pixel
	// has three bytes.
	qim = QImage(_tmp.data, _tmp.cols, _tmp.rows, _tmp.cols * 3, QImage::Format_RGB888);

	// Draw recent to top
	int topHeight = widgetSize.height() * updateRow / rowCount;
	QRectF target(0.0, 0.0, widgetSize.width(), widgetSize.height() * updateRow / rowCount);
	QRectF source(0.0, 0.0, rowWidth, updateRow);

	painter.translate(0, updateRow * widgetSize.height() / rowCount);
	painter.scale(1, -1);

	painter.drawImage(target, qim, source);

	// Draw old to bottom
	QRectF target2(0.0, 0.0, widgetSize.width(), widgetSize.height() - topHeight);
	QRectF source2(0.0, updateRow + 1, rowWidth, rowCount - updateRow - 1);

	painter.resetTransform();
	painter.translate(0, widgetSize.height());
	painter.scale(1, -1);

	painter.drawImage(target2, qim, source2);

	
	// Now just draw our label at the top.
	painter.resetTransform();
	painter.setBrush(QColor(0x30, 0x30, 0x30, 0xEE));
	painter.setPen(QColor("black"));
	// 33 instead of 30 as a hack. There are borders round the other widgets.
	painter.drawRect(QRectF(0.0, 0.0, this->width(), 33.0));

	painter.setBrush(QColor(0xFF, 0xFF, 0xFF, 0xFF));
	painter.setPen(QColor("white"));
	painter.scale(1.5, 1.5);
	painter.drawText(QRect(6, 4, 100, 100), "Belt Height Map");



	painter.end();
}
