#include "CaptureWorker.h"

#include <QDebug.h>

CaptureWorker::CaptureWorker(int cameraIndex = 0)
	: QObject()
{
	qDebug() << "Opening camera" << cameraIndex;
	capture.open(cameraIndex);

	capture.set(CV_CAP_PROP_FOURCC, CV_FOURCC('M', 'J', 'P', 'G'));
	capture.set(CV_CAP_PROP_FRAME_WIDTH, 1920);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, 1080);

	isCamera = true;

	if (!capture.isOpened()) {
		qDebug() << "Could not open camera" << cameraIndex;
		return;
	}

}

CaptureWorker::CaptureWorker(cv::String videoFile)
	: QObject()
{
	qDebug() << "Opening video: " << videoFile.c_str();
	capture.open(videoFile);

	isCamera = false;

	if (!capture.isOpened()) {
		qDebug() << "Could not open video: " << videoFile.c_str();
		return;
	}

}


CaptureWorker::~CaptureWorker()
{
	if (capture.isOpened()) {
		capture.release();
	}
}


void CaptureWorker::Start() {

	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(Capture()));
	if (isCamera){
		// run more frequently and test whether there's a new frame.
		timer->start(40);
	}
	else {
		// assume 25 frames per second
		timer->start(40);
	}

}

void CaptureWorker::Capture() {

	qDebug() << "In Capture...";

	if (capture.grab()) {
		capture.retrieve(frame);
		qDebug() << "Frame Width = " << frame.cols << "Frame Height = " << frame.rows;
		if (frame.cols == 0 || frame.rows == 0) {
			qDebug() << "Invalid frame skipping";
			return;
		}

		// Emit signal that we have captured a new frame.
		emit newFrame(frame);
	}
}