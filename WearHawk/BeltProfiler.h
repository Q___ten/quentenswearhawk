#pragma once

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <numeric>
#include <vector>

using namespace cv;

struct BeltProfileOutput {
	std::vector<float> pulleyVector;
	std::vector<float> thickness;
};

class BeltProfiler {

public:
	BeltProfiler();
	BeltProfiler(Rect beltROI, Rect PULLEY_LEFT_ROI, Rect PULLEY_RIGHT_ROI,
		int BELT_CANNY_LOWER, int PULLEY_LEFT_CANNY_LOWER, int PULLEY_RIGHT_CANNY_LOWER,
		int PULLEY_INSET);

	BeltProfileOutput processFrame(const Mat& frame);

private:

	Size frameSize;
	
	// Semi-Constants
	Rect BELT_ROI;
	Rect PULLEY_LEFT_ROI;
	Rect PULLEY_RIGHT_ROI;
	
	// Canny filter settings:
	int BELT_CANNY_LOWER;
	int PULLEY_LEFT_CANNY_LOWER;
	int PULLEY_RIGHT_CANNY_LOWER;

	int PULLEY_INSET;

	void printVector(const std::vector<int>& v);
	void printFloatVector(const std::vector<float>& v);
	Mat filterRegion(const Mat& frame, const Rect& region, int lowerThreshold);
	std::vector<int> pixelsFromTop(const Mat& binary);

};
