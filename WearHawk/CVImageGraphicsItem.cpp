#include "CVImageGraphicsItem.h"

#include <QDebug>

QRectF CVImageGraphicsItem::boundingRect() const
{	
	return QRectF(QPointF(0.0, 0.0), size);
}

/*
QPainterPath CVImageGraphicsItem::shape() const
{
	QPainterPath path;
	path.addRect(boundingRect());
	return path;
}
*/

void CVImageGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{		
	/*
	QSize size = widget->size();
	if (this->size != size) {
		this->size = size;
//		this->size = QSize(1920, 1080);
		prepareGeometryChange();
	}
	*/


	
//	painter->drawImage(QPoint(0, 0), _qimage.scaled(widget->size(), Qt::AspectRatioMode::IgnoreAspectRatio));
//	painter->resetTransform();
//	painter->scale(widget->size().width() / 1920.0, widget->size().height() / 1080.0);

//	painter->drawImage(QRect(QPoint(0, 0), widget->size()), _qimage);
	painter->drawImage(QPoint(0,0), _qimage);

	qDebug() << "drawing frame" << widget->size()  << " image size: " << _qimage.size();
}

/*
void CVImageGraphicsItem::mousePressEvent(QGraphicsSceneMouseEvent *event) 
{
	QGraphicsItem::mousePressEvent(event);
	//		update();
}

void CVImageGraphicsItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event) 
{
	QGraphicsItem::mouseMoveEvent(event);
}

void CVImageGraphicsItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsItem::mouseReleaseEvent(event);
	//		update();
}
*/

void CVImageGraphicsItem::showFrame(const cv::Mat& image) {
	// Convert the image to the RGB888 format
	switch (image.type()) {
	case CV_8UC1:
		cvtColor(image, _tmp, CV_GRAY2RGB);
		break;
	case CV_8UC3:
		cvtColor(image, _tmp, CV_BGR2RGB);
		break;
	}

	// QImage needs the data to be stored continuously in memory
	assert(_tmp.isContinuous());
	// Assign OpenCV's image buffer to the QImage. Note that the bytesPerLine parameter
	// (http://qt-project.org/doc/qt-4.8/qimage.html#QImage-6) is 3*width because each pixel
	// has three bytes.
	_qimage = QImage(_tmp.data, _tmp.cols, _tmp.rows, _tmp.cols * 3, QImage::Format_RGB888);

	//		this->setFixedSize(image.cols, image.rows);
	update();
}
