#pragma once

#include <QObject>
#include <QTimer.h>
#include <opencv2\core.hpp>
#include <opencv2\videoio.hpp>
#include <opencv2/highgui/highgui.hpp>  // Video write

class CaptureWorker : public QObject
{
	Q_OBJECT

public:
	CaptureWorker(int cameraIndex);
	CaptureWorker(cv::String videoFile);
	~CaptureWorker();

signals:
	void newFrame(const cv::Mat &newFrame);

public slots:
	void Start();

private:

	cv::VideoCapture capture;
	cv::Mat frame;
	
	QTimer *timer;

	bool isCamera;

private slots:

	void Capture();

};
