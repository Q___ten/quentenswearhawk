# README #

### This repo is Quenten's attempt at getting git to work without breaking what's already there. ###
We'll consolidate later I think.

You need Qt and OpenCV installed.

You'll need to define OpenCV_DIR and QTDIR in the environment variables. Hopefully there are no more dependencies than that to get it to compile.

You'll also need a Videos directory in the top level directory, alongside the WearHawk directory and the WearHawk.sln file. Put some videos in there. Like 00003.MTS.

I've gotten it compiling and working up to a point.

Good luck!